

     ************************************************************************
     *************** Dalton - An Electronic Structure Program ***************
     ************************************************************************

    This is output from DALTON (Release Dalton2013 patch 0)
   ----------------------------------------------------------------------------
    NOTE:
     
    Dalton is an experimental code for the evaluation of molecular
    properties using (MC)SCF, DFT, CI, and CC wave functions.
    The authors accept no responsibility for the performance of
    the code or for the correctness of the results.
     
    The code (in whole or part) is provided under a licence and
    is not to be reproduced for further distribution without
    the written permission of the authors or their representatives.
     
    See the home page "http://daltonprogram.org" for further information.
     
    If results obtained with this code are published,
    the appropriate citations would be both of:
     
       K. Aidas, C. Angeli, K. L. Bak, V. Bakken, R. Bast,
       L. Boman, O. Christiansen, R. Cimiraglia, S. Coriani,
       P. Dahle, E. K. Dalskov, U. Ekstroem, T. Enevoldsen,
       J. J. Eriksen, P. Ettenhuber, B. Fernandez, L. Ferrighi,
       H. Fliegl, L. Frediani, K. Hald, A. Halkier, C. Haettig,
       H. Heiberg, T. Helgaker, A. C. Hennum, H. Hettema,
       E. Hjertenaes, S. Hoest, I.-M. Hoeyvik, M. F. Iozzi,
       B. Jansik, H. J. Aa. Jensen, D. Jonsson, P. Joergensen,
       J. Kauczor, S. Kirpekar, T. Kjaergaard, W. Klopper,
       S. Knecht, R. Kobayashi, H. Koch, J. Kongsted, A. Krapp,
       K. Kristensen, A. Ligabue, O. B. Lutnaes, J. I. Melo,
       K. V. Mikkelsen, R. H. Myhre, C. Neiss, C. B. Nielsen,
       P. Norman, J. Olsen, J. M. H. Olsen, A. Osted,
       M. J. Packer, F. Pawlowski, T. B. Pedersen, P. F. Provasi,
       S. Reine, Z. Rinkevicius, T. A. Ruden, K. Ruud, V. Rybkin,
       P. Salek, C. C. M. Samson, A. Sanchez de Meras, T. Saue,
       S. P. A. Sauer, B. Schimmelpfennig, K. Sneskov,
       A. H. Steindal, K. O. Sylvester-Hvid, P. R. Taylor,
       A. M. Teale, E. I. Tellgren, D. P. Tew, A. J. Thorvaldsen,
       L. Thoegersen, O. Vahtras, M. A. Watson, D. J. D. Wilson,
       M. Ziolkowski and H. Agren.
       The Dalton quantum chemistry program system.
       WIREs Comput. Mol. Sci. 2013. doi: 10.1002/wcms.1172
    
    and
    
       Dalton, a Molecular Electronic Structure Program,
       Release DALTON2013.0 (2013), see http://daltonprogram.org
   ----------------------------------------------------------------------------

    Authors in alphabetical order (major contribution(s) in parenthesis):

  Kestutis Aidas,           Vilnius University,           Lithuania   (QM/MM)
  Celestino Angeli,         University of Ferrara,        Italy       (NEVPT2)
  Keld L. Bak,              UNI-C,                        Denmark     (AOSOPPA, non-adiabatic coupling, magnetic properties)
  Vebjoern Bakken,          University of Oslo,           Norway      (DALTON; geometry optimizer, symmetry detection)
  Radovan Bast,             KTH Stockholm                 Sweden      (DALTON installation and execution frameworks)
  Linus Boman,              NTNU,                         Norway      (Cholesky decomposition and subsystems)
  Ove Christiansen,         Aarhus University,            Denmark     (CC module)
  Renzo Cimiraglia,         University of Ferrara,        Italy       (NEVPT2)
  Sonia Coriani,            University of Trieste,        Italy       (CC module, MCD in RESPONS)
  Paal Dahle,               University of Oslo,           Norway      (Parallelization)
  Erik K. Dalskov,          UNI-C,                        Denmark     (SOPPA)
  Thomas Enevoldsen,        Univ. of Southern Denmark,    Denmark     (SOPPA)
  Janus J. Eriksen,         Aarhus University,            Denmark     (PE-MP2/SOPPA, TDA)
  Berta Fernandez,          U. of Santiago de Compostela, Spain       (doublet spin, ESR in RESPONS)
  Lara Ferrighi,            Aarhus University,            Denmark     (PCM Cubic response)
  Heike Fliegl,             University of Oslo,           Norway      (CCSD(R12))
  Luca Frediani,            UiT The Arctic U. of Norway,  Norway      (PCM)
  Bin Gao,                  UiT The Arctic U. of Norway,  Norway      (Gen1Int library)
  Christof Haettig,         Ruhr-University Bochum,       Germany     (CC module)
  Kasper Hald,              Aarhus University,            Denmark     (CC module)
  Asger Halkier,            Aarhus University,            Denmark     (CC module)
  Hanne Heiberg,            University of Oslo,           Norway      (geometry analysis, selected one-electron integrals)
  Trygve Helgaker,          University of Oslo,           Norway      (DALTON; ABACUS, ERI, DFT modules, London, and much more)
  Alf Christian Hennum,     University of Oslo,           Norway      (Parity violation)
  Hinne Hettema,            University of Auckland,       New Zealand (quadratic response in RESPONS; SIRIUS supersymmetry)
  Eirik Hjertenaes,         NTNU,                         Norway      (Cholesky decomposition)
  Maria Francesca Iozzi,    University of Oslo,           Norway      (RPA)
  Brano Jansik              Technical Univ. of Ostrava    Czech Rep.  (DFT cubic response)
  Hans Joergen Aa. Jensen,  Univ. of Southern Denmark,    Denmark     (DALTON; SIRIUS, RESPONS, ABACUS modules, London, and much more)
  Dan Jonsson,              UiT The Arctic U. of Norway,  Norway      (cubic response in RESPONS module)
  Poul Joergensen,          Aarhus University,            Denmark     (RESPONS, ABACUS, and CC modules)
  Joanna Kauczor,           Linkoeping University,        Sweden      (Complex polarization propagator (CPP) module)
  Sheela Kirpekar,          Univ. of Southern Denmark,    Denmark     (Mass-velocity & Darwin integrals)
  Wim Klopper,              KIT Karlsruhe,                Germany     (R12 code in CC, SIRIUS, and ABACUS modules)
  Stefan Knecht,            ETH Zurich,                   Switzerland (Parallel CI and MCSCF)
  Rika Kobayashi,           Australian National Univ.,    Australia   (DIIS in CC, London in MCSCF)
  Henrik Koch,              NTNU,                         Norway      (CC module, Cholesky decomposition)
  Jacob Kongsted,           Univ. of Southern Denmark,    Denmark     (Polarizable embedding, QM/MM)
  Andrea Ligabue,           University of Modena,         Italy       (CTOCD, AOSOPPA)
  Ola B. Lutnaes,           University of Oslo,           Norway      (DFT Hessian)
  Juan I. Melo,             University of Buenos Aires,   Argentina   (LRESC, Relativistic Effects on NMR Shieldings)
  Kurt V. Mikkelsen,        University of Copenhagen,     Denmark     (MC-SCRF and QM/MM)
  Rolf H. Myhre,            NTNU,                         Norway      (Cholesky, subsystems and ECC2)
  Christian Neiss,          Univ. Erlangen-Nuernberg,     Germany     (CCSD(R12))
  Christian B. Nielsen,     University of Copenhagen,     Denmark     (QM/MM)
  Patrick Norman,           Linkoeping University,        Sweden      (Cubic response and complex response in RESPONS)
  Jeppe Olsen,              Aarhus University,            Denmark     (SIRIUS CI/density modules)
  Jogvan Magnus H. Olsen,   Univ. of Southern Denmark,    Denmark     (Polarizable embedding, PE library, QM/MM)
  Anders Osted,             Copenhagen University,        Denmark     (QM/MM)
  Martin J. Packer,         University of Sheffield,      UK          (SOPPA)
  Filip Pawlowski,          Kazimierz Wielki University   Poland      (CC3)
  Thomas B. Pedersen,       University of Oslo,           Norway      (Cholesky decomposition)
  Patricio F. Provasi,      University of Northeastern,   Argentina   (Analysis of coupling constants in localized orbitals)
  Zilvinas Rinkevicius,     KTH Stockholm,                Sweden      (open-shell DFT, ESR)
  Elias Rudberg,            KTH Stockholm,                Sweden      (DFT grid and basis info)
  Torgeir A. Ruden,         University of Oslo,           Norway      (Numerical derivatives in ABACUS)
  Kenneth Ruud,             UiT The Arctic U. of Norway,  Norway      (DALTON; ABACUS magnetic properties and  much more)
  Pawel Salek,              KTH Stockholm,                Sweden      (DALTON; DFT code)
  Claire C. M. Samson       University of Karlsruhe       Germany     (Boys localization, r12 integrals in ERI)
  Alfredo Sanchez de Meras, University of Valencia,       Spain       (CC module, Cholesky decomposition)
  Trond Saue,               Paul Sabatier University,     France      (direct Fock matrix construction)
  Stephan P. A. Sauer,      University of Copenhagen,     Denmark     (SOPPA(CCSD), SOPPA prop., AOSOPPA, vibrational g-factors)
  Bernd Schimmelpfennig,    Forschungszentrum Karlsruhe,  Germany     (AMFI module)
  Kristian Sneskov,         Aarhus University,            Denmark     (QM/MM, PE-CC)
  Arnfinn H. Steindal,      UiT The Arctic U. of Norway,  Norway      (parallel QM/MM)
  K. O. Sylvester-Hvid,     University of Copenhagen,     Denmark     (MC-SCRF)
  Peter R. Taylor,          VLSCI/Univ. of Melbourne,     Australia   (Symmetry handling ABACUS, integral transformation)
  Andrew M. Teale,          University of Nottingham,     England     (DFT-AC, DFT-D)
  David P. Tew,             University of Bristol,        England     (CCSD(R12))
  Olav Vahtras,             KTH Stockholm,                Sweden      (triplet response, spin-orbit, ESR, TDDFT, open-shell DFT)
  David J. Wilson,          La Trobe University,          Australia   (DFT Hessian and DFT magnetizabilities)
  Hans Agren,               KTH Stockholm,                Sweden      (SIRIUS module, RESPONS, MC-SCRF solvation model)
 --------------------------------------------------------------------------------

     Date and time (Linux)  : Sun Sep  8 20:38:41 2013
     Host name              : lpqlx131.ups-tlse.fr                    

 * Work memory size             :    64000000 =  488.28 megabytes.

 * Directories for basis set searches:
   1) /home/bast/DALTON-2013.0-Source/build/test_tddft_tda
   2) /home/bast/DALTON-2013.0-Source/build/basis


Compilation information
-----------------------

 Who compiled             | bast
 Host                     | lpqlx131.ups-tlse.fr
 System                   | Linux-3.8.5-201.fc18.x86_64
 CMake generator          | Unix Makefiles
 Processor                | x86_64
 64-bit integers          | OFF
 MPI                      | OFF
 Fortran compiler         | /usr/bin/gfortran
 Fortran compiler version | GNU Fortran (GCC) 4.7.2 20121109 (Red Hat 4.7.2-8)
 C compiler               | /usr/bin/gcc
 C compiler version       | gcc (GCC) 4.7.2 20121109 (Red Hat 4.7.2-8)
 C++ compiler             | /usr/bin/g++
 C++ compiler version     | g++ (GCC) 4.7.2 20121109 (Red Hat 4.7.2-8)
 Static linking           | OFF
 Last Git revision        | f34203295a86316e27f9e7b44f9b6769c4a046c0
 Configuration time       | 2013-09-08 20:31:27.952056


   Content of the .dal input file
 ----------------------------------

*DALTON INPUT                                     
.RUN RESPONSE                                     
**WAVE FUNCTIONS                                  
.DFT                                              
 Combine Slater=1 Becke=1 VWN=1 P86c=1            
*SCF INPUT                                        
.THRESH                                           
 1.0D-06                                          
**RESPONSE                                        
*LINEAR                                           
.TDA                                              
.SINGLE RESIDUE                                   
.DIPLEN                                           
.ROOTS                                            
 3                                                
**END OF INPUT                                    


   Content of the .mol file
 ----------------------------

ATOMBASIS                                                                      
Energy calculation without symmetry                                            
Basis set specified with ATOMBASIS                                             
    2    0                                                                     
        8.    1      Basis=cc-pVDZ                                             
O     0.0 -0.2249058930 0.0                                                    
        1.    2      Basis=cc-pVDZ                                             
H1    1.45235 0.899623 0.0                                                     
H2   -1.45235 0.899623 0.0                                                     


       *******************************************************************
       *********** Output from DALTON general input processing ***********
       *******************************************************************

 --------------------------------------------------------------------------------
   Overall default print level:    0
   Print level for DALTON.STAT:    1

    HERMIT 1- and 2-electron integral sections will be executed
    "Old" integral transformation used (limited to max 255 basis functions)
    Wave function sections will be executed (SIRIUS module)
    Dynamic molecular response properties section will be executed (RESPONSE module)
 --------------------------------------------------------------------------------


   ****************************************************************************
   *************** Output of molecule and basis set information ***************
   ****************************************************************************


    The two title cards from your ".mol" input:
    ------------------------------------------------------------------------
 1: Energy calculation without symmetry                                     
 2: Basis set specified with ATOMBASIS                                      
    ------------------------------------------------------------------------

  Atomic type no.    1
  --------------------
  Nuclear charge:   8.00000
  Number of symmetry independent centers:    1
  Number of basis sets to read;    2
  The basis set is "cc-pVDZ" from the basis set library.
  Basis set file used for this atomic type with Z =   8 :
     "/home/bast/DALTON-2013.0-Source/build/basis/cc-pVDZ"

  Atomic type no.    2
  --------------------
  Nuclear charge:   1.00000
  Number of symmetry independent centers:    2
  Number of basis sets to read;    2
  The basis set is "cc-pVDZ" from the basis set library.
  Basis set file used for this atomic type with Z =   1 :
     "/home/bast/DALTON-2013.0-Source/build/basis/cc-pVDZ"


                         SYMGRP: Point group information
                         -------------------------------

Point group: C1 


                                 Isotopic Masses
                                 ---------------

                           O          15.994915
                           H1          1.007825
                           H2          1.007825

                       Total mass:    18.010565 amu
                       Natural abundance:  99.730 %

 Center-of-mass coordinates (a.u.):    0.000000   -0.099054    0.000000


  Atoms and basis sets
  --------------------

  Number of atom types :    2
  Total number of atoms:    3

  label    atoms   charge   prim   cont     basis
  ----------------------------------------------------------------------
  O           1    8.0000    26    14      [9s4p1d|3s2p1d]                                    
  H2          2    1.0000     7     5      [4s1p|2s1p]                                        
  ----------------------------------------------------------------------
  total:      3   10.0000    40    24
  ----------------------------------------------------------------------
  Spherical harmonic basis used.

  Threshold for neglecting AO integrals:  1.00D-12


  Cartesian Coordinates (a.u.)
  ----------------------------

  Total number of coordinates:    9
  O       :     1  x   0.0000000000    2  y  -0.2249058930    3  z   0.0000000000
  H1      :     4  x   1.4523500000    5  y   0.8996230000    6  z   0.0000000000
  H2      :     7  x  -1.4523500000    8  y   0.8996230000    9  z   0.0000000000


   Interatomic separations (in Angstrom):
   --------------------------------------

            O           H1          H2    
            ------      ------      ------
 O     :    0.000000
 H1    :    0.972000    0.000000
 H2    :    0.972000    1.537101    0.000000


  Max    interatomic separation is    1.5371 Angstrom (    2.9047 Bohr)
  between atoms    3 and    2, "H2    " and "H1    ".

  Min HX interatomic separation is    0.9720 Angstrom (    1.8368 Bohr)


  Bond distances (Angstrom):
  --------------------------

                  atom 1     atom 2       distance
                  ------     ------       --------
  bond distance:  H1         O            0.972000
  bond distance:  H2         O            0.972000


  Bond angles (degrees):
  ----------------------

                  atom 1     atom 2     atom 3         angle
                  ------     ------     ------         -----
  bond angle:     H1         O          H2           104.500




 Principal moments of inertia (u*A**2) and principal axes
 --------------------------------------------------------

   IA       0.633889          1.000000    0.000000    0.000000
   IB       1.190584          0.000000    1.000000    0.000000
   IC       1.824473          0.000000    0.000000    1.000000


 Rotational constants
 --------------------

 The molecule is planar.

               A                   B                   C

         797267.3370         424479.9976         277000.0210 MHz
           26.593976           14.159129            9.239726 cm-1


@  Nuclear repulsion energy :    9.055004525638 Hartree


                     .---------------------------------------.
                     | Starting in Integral Section (HERMIT) |
                     `---------------------------------------'



    *************************************************************************
    ****************** Output from HERMIT input processing ******************
    *************************************************************************



     ************************************************************************
     ************************** Output from HERINT **************************
     ************************************************************************


 Threshold for neglecting two-electron integrals:  1.00D-12
 Number of two-electron integrals written:       21669 ( 48.0% )
 Megabytes written:                              0.254

 >>>> Total CPU  time used in HERMIT:   0.03 seconds
 >>>> Total wall time used in HERMIT:   0.03 seconds


                        .----------------------------------.
                        | End of Integral Section (HERMIT) |
                        `----------------------------------'



                   .--------------------------------------------.
                   | Starting in Wave Function Section (SIRIUS) |
                   `--------------------------------------------'


 *** Output from Huckel module :

     Using EWMO model:          T
     Using EHT  model:          F
     Number of Huckel orbitals each symmetry:    7

 EWMO - Energy Weighted Maximum Overlap - is a Huckel type method,
        which normally is better than Extended Huckel Theory.
 Reference: Linderberg and Ohrn, Propagators in Quantum Chemistry (Wiley, 1973)

 Huckel EWMO eigenvalues for symmetry :  1
          -20.684736      -1.601749      -0.777914      -0.689171      -0.616200
           -0.237251      -0.172878

 **********************************************************************
 *SIRIUS* a direct, restricted step, second order MCSCF program       *
 **********************************************************************

 
     Date and time (Linux)  : Sun Sep  8 20:38:41 2013
     Host name              : lpqlx131.ups-tlse.fr                    

 Title lines from ".mol" input file:
     Energy calculation without symmetry                                     
     Basis set specified with ATOMBASIS                                      

 Print level on unit LUPRI =   2 is   0
 Print level on unit LUW4  =   2 is   5

@    Restricted, closed shell Kohn-Sham DFT calculation.

@    Time-dependent Kohn-Sham DFT calculation (TD-DFT).

 Initial molecular orbitals are obtained according to
 ".MOSTART EWMO  " input option

     Wave function specification
     ============================
@    For the wave function of type :      >>> KS-DFT <<<
@    Number of closed shell electrons          10
@    Number of electrons in active shells       0
@    Total charge of the molecule               0

@    Spin multiplicity and 2 M_S                1         0
     Total number of symmetries                 1
@    Reference state symmetry                   1
 
     This is a DFT calculation of type: Combine
 Weighted mixed functional:
                      P86c:    1.00000
                       VWN:    1.00000
                     Becke:    1.00000
                    Slater:    1.00000

     Orbital specifications
     ======================
     Abelian symmetry species          All |    1
                                       --- |  ---
@    Occupied SCF orbitals               5 |    5
     Secondary orbitals                 19 |   19
     Total number of orbitals           24 |   24
     Number of basis functions          24 |   24

     Optimization information
     ========================
@    Number of configurations                 1
@    Number of orbital rotations             95
     ------------------------------------------
@    Total number of variables               96

     Maximum number of Fock   iterations      0
     Maximum number of DIIS   iterations     60
     Maximum number of QC-SCF iterations     60
     Threshold for SCF convergence     1.00D-06
 
     This is a DFT calculation of type: Combine
 Weighted mixed functional:
                      P86c:    1.00000
                       VWN:    1.00000
                     Becke:    1.00000
                    Slater:    1.00000


 >>>>> DIIS optimization of Hartree-Fock <<<<<

 C1-DIIS algorithm; max error vectors =   10

 Iter      Total energy        Error norm    Delta(E)  DIIS dim.
 -----------------------------------------------------------------------------
      K-S energy, electrons, error :     -9.283136829340   9.9999996460   -3.54D-07
@  1    -76.2608845576        1.60458D+00   -7.63D+01    1
      Virial theorem: -V/T =      2.005654
@      MULPOP O      -0.55; H1      0.28; H2      0.28; 
 -----------------------------------------------------------------------------
      K-S energy, electrons, error :     -9.359354409378   9.9999996810   -3.19D-07
@  2    -76.4020223879        7.31276D-01   -1.41D-01    2
      Virial theorem: -V/T =      2.005774
@      MULPOP O      -0.14; H1      0.07; H2      0.07; 
 -----------------------------------------------------------------------------
      K-S energy, electrons, error :     -9.390371391269   9.9999996712   -3.29D-07
@  3    -76.4241263493        1.64054D-01   -2.21D-02    3
      Virial theorem: -V/T =      2.003101
@      MULPOP O      -0.31; H1      0.15; H2      0.15; 
 -----------------------------------------------------------------------------
      K-S energy, electrons, error :     -9.327313903546   9.9999996742   -3.26D-07
@  4    -76.4242356160        1.61329D-01   -1.09D-04    4
      Virial theorem: -V/T =      2.008803
@      MULPOP O      -0.23; H1      0.12; H2      0.12; 
 -----------------------------------------------------------------------------
      K-S energy, electrons, error :     -9.358718669551   9.9999996729   -3.27D-07
@  5    -76.4253885780        1.75397D-03   -1.15D-03    5
      Virial theorem: -V/T =      2.005911
@      MULPOP O      -0.27; H1      0.14; H2      0.14; 
 -----------------------------------------------------------------------------
      K-S energy, electrons, error :     -9.358870825907   9.9999996729   -3.27D-07
@  6    -76.4253887273        9.31225D-05   -1.49D-07    6
      Virial theorem: -V/T =      2.005899
@      MULPOP O      -0.27; H1      0.14; H2      0.14; 
 -----------------------------------------------------------------------------
      K-S energy, electrons, error :     -9.358888425233   9.9999996729   -3.27D-07
@  7    -76.4253887277        1.81184D-06   -3.92D-10    7
      Virial theorem: -V/T =      2.005897
@      MULPOP O      -0.27; H1      0.14; H2      0.14; 
 -----------------------------------------------------------------------------
      K-S energy, electrons, error :     -9.358888274777   9.9999996729   -3.27D-07
@  8    -76.4253887277        1.36015D-07   -4.55D-13    8

@ *** DIIS converged in   8 iterations !
@     Converged SCF energy, gradient:    -76.425388727676    1.36D-07
    - total time used in SIRFCK :              0.00 seconds


 *** SCF orbital energy analysis ***

 Only the five lowest virtual orbital energies printed in each symmetry.

 Number of electrons :   10
 Orbital occupations :    5

 Sym       Kohn-Sham orbital energies

  1    -18.76494131    -0.90183022    -0.45706864    -0.30813173    -0.22912144
         0.03425762     0.10956342     0.51219692     0.55839993     0.84631653

    E(LUMO) :     0.03425762 au (symmetry 1)
  - E(HOMO) :    -0.22912144 au (symmetry 1)
  ------------------------------------------
    gap     :     0.26337905 au

 >>> Writing SIRIFC interface file <<<

 >>>> CPU and wall time for SCF :       3.356       3.390


                       .-----------------------------------.
                       | >>> Final results from SIRIUS <<< |
                       `-----------------------------------'


@    Spin multiplicity:           1
@    Spatial symmetry:            1
@    Total charge of molecule:    0

@    Final DFT energy:            -76.425388727676                 
@    Nuclear repulsion:             9.055004525638
@    Electronic energy:           -85.480393253314

@    Final gradient norm:           0.000000136015

 
     Date and time (Linux)  : Sun Sep  8 20:38:45 2013
     Host name              : lpqlx131.ups-tlse.fr                    

 (Only coefficients >0.0100 are printed.)

 Molecular orbitals for symmetry species  1
 ------------------------------------------

    Orbital         1        2        3        4        5        6        7
   1 O   :1s    -1.0030   0.0063  -0.0000  -0.0081   0.0000  -0.0198   0.0000
   2 O   :1s    -0.0166  -0.8658  -0.0000   0.3345   0.0000   0.3049   0.0000
   3 O   :1s     0.0148   0.1403   0.0000   0.1900  -0.0000   0.6675  -0.0000
   4 O   :2px    0.0000  -0.0000  -0.7340  -0.0000   0.0000   0.0000  -0.5883
   5 O   :2py   -0.0028  -0.1782   0.0000  -0.7906   0.0000   0.3956   0.0000
   6 O   :2pz    0.0000  -0.0000  -0.0000   0.0000  -0.9241   0.0000  -0.0000
   7 O   :2px    0.0000   0.0000   0.1386   0.0000  -0.0000   0.0000  -0.3224
   8 O   :2py    0.0022   0.0838   0.0000   0.0115  -0.0000   0.1219  -0.0000
   9 O   :2pz   -0.0000  -0.0000  -0.0000   0.0000  -0.0653   0.0000  -0.0000
  10 O   :3d2-   0.0000  -0.0000  -0.0218   0.0000  -0.0000   0.0000  -0.0034
  11 O   :3d1-  -0.0000  -0.0000  -0.0000   0.0000  -0.0193   0.0000  -0.0000
  12 O   :3d0    0.0004   0.0058  -0.0000   0.0101   0.0000  -0.0044   0.0000
  14 O   :3d2+  -0.0002  -0.0003   0.0000   0.0154   0.0000  -0.0073  -0.0000
  15 H1  :1s     0.0006  -0.3444  -0.5618  -0.3419   0.0000  -0.2404   0.2575
  16 H1  :1s    -0.0014   0.1575   0.1730   0.1204   0.0000  -0.6075   1.0896
  17 H1  :2px   -0.0009   0.0362   0.0134   0.0231  -0.0000  -0.0158   0.0081
  18 H1  :2py   -0.0009   0.0179   0.0268  -0.0173  -0.0000  -0.0030   0.0136
  19 H1  :2pz   -0.0000   0.0000   0.0000  -0.0000  -0.0338  -0.0000   0.0000
  20 H2  :1s     0.0006  -0.3444   0.5618  -0.3419   0.0000  -0.2404  -0.2575
  21 H2  :1s    -0.0014   0.1575  -0.1730   0.1204  -0.0000  -0.6075  -1.0896
  22 H2  :2px    0.0009  -0.0362   0.0134  -0.0231  -0.0000   0.0158   0.0081
  23 H2  :2py   -0.0009   0.0179  -0.0268  -0.0173   0.0000  -0.0030  -0.0136
  24 H2  :2pz   -0.0000   0.0000   0.0000  -0.0000  -0.0338  -0.0000   0.0000



 >>>> Total CPU  time used in SIRIUS :      3.36 seconds
 >>>> Total wall time used in SIRIUS :      3.40 seconds

 
     Date and time (Linux)  : Sun Sep  8 20:38:45 2013
     Host name              : lpqlx131.ups-tlse.fr                    


                     .---------------------------------------.
                     | End of Wave Function Section (SIRIUS) |
                     `---------------------------------------'



                 .------------------------------------------------.
                 | Starting in Dynamic Property Section (RESPONS) |
                 `------------------------------------------------'


 ------------------------------------------------------------------------------
  RESPONSE  -  an MCSCF, MC-srDFT, DFT, and SOPPA response property program
 ------------------------------------------------------------------------------


 <<<<<<<<<< OUTPUT FROM RESPONSE INPUT PROCESSING >>>>>>>>>>




  Linear Response single residue calculation
 -------------------------------------------

 Print level                                    : IPRPP  =   2
 Maximum number of iterations for eigenval.eqs. : MAXITP =  60
 Threshold for convergence of eigenvalue eqs.   : THCPP  = 1.000D-03
 Maximum iterations in optimal orbital algorithm: MAXITO =   5

 TAMM-DANCOFF APPROXIMATION (B = 0). TDA=T

      3 Excitation energies are calculated for symmetry no.    1

      3 property residues are calculated with labels:

               XDIPLEN 
               YDIPLEN 
               ZDIPLEN 

 Integral transformation: Total CPU and WALL times (sec)       0.008       0.007


   SCF energy         :      -76.425388727676122
 -- inactive part     :      -85.480393253314020
 -- nuclear repulsion :        9.055004525637894


                    *****************************************
                    *** DFT response calculation (TD-DFT) ***
                    *****************************************



 >>>>>>>>>> Linear response calculation
 >>>>>>>>>> Symmetry of excitation/property operator(s)    1

 Number of excitations of this symmetry            3
 Number of response properties of this symmetry    0
 Number of C6/C8 properties of this symmetry       0


 Perturbation symmetry.     KSYMOP:       1
 Perturbation spin symmetry.TRPLET:       F
 Orbital variables.         KZWOPT:      95
 Configuration variables.   KZCONF:       0
 Total number of variables. KZVAR :      95
 Electrons in DFTMOMO:    9.99999967292250



 <<< EXCITATION ENERGIES AND TRANSITION MOMENT CALCULATION (MCTDHF) >>>

 Operator symmetry =  1; triplet =   F


 *** THE REQUESTED    3 SOLUTION VECTORS CONVERGED

 Convergence of RSP solution vectors, threshold = 1.00D-03
 ---------------------------------------------------------------
 (dimension of paired reduced space:   18)
 RSP solution vector no.    1; norm of residual   1.41D-04
 RSP solution vector no.    2; norm of residual   3.78D-04
 RSP solution vector no.    3; norm of residual   1.09D-04

 *** RSPCTL MICROITERATIONS CONVERGED

@ Transition operator type:    XDIPLEN 
@ STATE NO:    1 *TRANSITION MOMENT:  2.31291097E-16 *ENERGY(eV):   7.4479560    
@ STATE NO:    2 *TRANSITION MOMENT: -7.47824092E-17 *ENERGY(eV):   9.3286223    
@ STATE NO:    3 *TRANSITION MOMENT:  3.21209778E-14 *ENERGY(eV):   9.7891164    

@ Transition operator type:    YDIPLEN 
@ STATE NO:    1 *TRANSITION MOMENT:  1.16375397E-16 *ENERGY(eV):   7.4479560    
@ STATE NO:    2 *TRANSITION MOMENT:  6.01235202E-16 *ENERGY(eV):   9.3286223    
@ STATE NO:    3 *TRANSITION MOMENT:  0.61248415     *ENERGY(eV):   9.7891164    

@ Transition operator type:    ZDIPLEN 
@ STATE NO:    1 *TRANSITION MOMENT: -0.34653498     *ENERGY(eV):   7.4479560    
@ STATE NO:    2 *TRANSITION MOMENT: -2.75407236E-14 *ENERGY(eV):   9.3286223    
@ STATE NO:    3 *TRANSITION MOMENT:  2.84307861E-18 *ENERGY(eV):   9.7891164    


  ******************************************************************************
  *** @ Excit. operator sym 1 & ref. state sym 1 => excited state symmetry 1 ***
  ******************************************************************************



 @ Excited state no:    1 in symmetry  1
 ---------------------------------------

@ Excitation energy :  0.27370735    au
@                      7.4479560     eV
@                      60071.820     cm-1
                       718.61855     kJ / mol

@ Total energy :      -76.151681     au

@ Operator type:    XDIPLEN 
@ Oscillator strength (LENGTH)   :  9.76142086E-33  (Transition moment :  2.31291097E-16 )

@ Operator type:    YDIPLEN 
@ Oscillator strength (LENGTH)   :  2.47125496E-33  (Transition moment :  1.16375397E-16 )

@ Operator type:    ZDIPLEN 
@ Oscillator strength (LENGTH)   :  2.19123710E-02  (Transition moment : -0.34653498     )


                            PBHT MO Overlap Diagnostic
                            --------------------------

  Reference: MJG Peach, P Benfield, T Helgaker, and DJ Tozer.
             J Chem Phys 128, 044118 (2008)


  The dominant contributions:

      I    A    K_IA      K_AI   <|I|*|A|> <I^2*A^2>    Weight   Contrib

      5    6 -0.706657  0.000000  0.436393  0.330466  0.499364  0.217919

@ Overlap diagnostic LAMBDA =    0.4365


 @ Excited state no:    2 in symmetry  1
 ---------------------------------------

@ Excitation energy :  0.34282057    au
@                      9.3286223     eV
@                      75240.418     cm-1
                       900.07527     kJ / mol

@ Total energy :      -76.082568     au

@ Operator type:    XDIPLEN 
@ Oscillator strength (LENGTH)   :  1.27812851E-33  (Transition moment : -7.47824092E-17 )

@ Operator type:    YDIPLEN 
@ Oscillator strength (LENGTH)   :  8.26160483E-32  (Transition moment :  6.01235202E-16 )

@ Operator type:    ZDIPLEN 
@ Oscillator strength (LENGTH)   :  1.73350983E-28  (Transition moment : -2.75407236E-14 )


                            PBHT MO Overlap Diagnostic
                            --------------------------

  Reference: MJG Peach, P Benfield, T Helgaker, and DJ Tozer.
             J Chem Phys 128, 044118 (2008)


  The dominant contributions:

      I    A    K_IA      K_AI   <|I|*|A|> <I^2*A^2>    Weight   Contrib

      5    7 -0.706999  0.000000  0.374993  0.284895  0.499847  0.187439

@ Overlap diagnostic LAMBDA =    0.3750


 @ Excited state no:    3 in symmetry  1
 ---------------------------------------

@ Excitation energy :  0.35974342    au
@                      9.7891164     eV
@                      78954.553     cm-1
                       944.50620     kJ / mol

@ Total energy :      -76.065645     au

@ Operator type:    XDIPLEN 
@ Oscillator strength (LENGTH)   :  2.47445245E-28  (Transition moment :  3.21209778E-14 )

@ Operator type:    YDIPLEN 
@ Oscillator strength (LENGTH)   :  8.99686711E-02  (Transition moment :  0.61248415     )

@ Operator type:    ZDIPLEN 
@ Oscillator strength (LENGTH)   :  1.93856039E-36  (Transition moment :  2.84307861E-18 )


                            PBHT MO Overlap Diagnostic
                            --------------------------

  Reference: MJG Peach, P Benfield, T Helgaker, and DJ Tozer.
             J Chem Phys 128, 044118 (2008)


  The dominant contributions:

      I    A    K_IA      K_AI   <|I|*|A|> <I^2*A^2>    Weight   Contrib

      4    6  0.700623  0.000000  0.511301  0.323008  0.490872  0.250983

@ Overlap diagnostic LAMBDA =    0.5142


 Time used in polarization propagator calculation is      4.37 CPU seconds for symmetry 1

 >>>> Total CPU  time used in RESPONSE:   4.38 seconds
 >>>> Total wall time used in RESPONSE:   4.41 seconds


                   .-------------------------------------------.
                   | End of Dynamic Property Section (RESPONS) |
                   `-------------------------------------------'

 >>>> Total CPU  time used in DALTON:   7.77 seconds
 >>>> Total wall time used in DALTON:   7.84 seconds

 
     Date and time (Linux)  : Sun Sep  8 20:38:49 2013
     Host name              : lpqlx131.ups-tlse.fr                    
